import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendForm from './AttendForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from "react-router-dom"


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
  <BrowserRouter>
    <Nav />
    <div>
      <Routes>
        <Route path ="attendees" element={<AttendeesList attendees={props.attendees}/>} />
        <Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route>
            <Route path="attendees" >
              <Route path="new" element={<AttendForm />} />
            </Route>
            <Route>
              <Route path="conferences">
                <Route path="new" element={<ConferenceForm />} />
              </Route>
              <Route>
                <Route path="presentations">
                  <Route path="new" element={<PresentationForm />} />
                </Route>
                <Route index element={<MainPage />} />
              </Route>
            </Route>
          </Route>
        </Route>
      </Routes>
    </div>
  </BrowserRouter>
  );
}

export default App;


      {/* <ConferenceForm /> */}
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}
