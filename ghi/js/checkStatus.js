const payloadCookie = await cookieStore.get("jwt_access_payload")
if(payloadCookie) {
    const encodedPayload = JSON.parse(payloadCookie.value);
    const decodedPayload = atob(encodedPayload);
    const payload = JSON.parse(decodedPayload);
    const permissions = payload.user.perms;
    if (permissions.includes('events.add_conference')) {
        document.getElementById('conferencePermissions').classList.remove('d-none');
    if (permissions.includes('events.add_location')) {
        document.getElementById('locationPermissions').classList.remove('d-none');
    }
}
}
